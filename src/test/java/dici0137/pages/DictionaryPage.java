package dici0137.pages;

import net.bytebuddy.asm.Advice;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import net.serenitybdd.core.pages.WebElementFacade;
import java.util.stream.Collectors;

import net.serenitybdd.core.annotations.findby.FindBy;

import net.thucydides.core.pages.PageObject;

import java.util.List;

@DefaultUrl("https://www.gutenberg.org/wiki/Main_Page")
public class DictionaryPage extends PageObject {

    @FindBy(name="query")
    private WebElementFacade searchTerms;

    @FindBy(xpath="//*[@id=\"p-lang\"]/div/ul/li[1]/a")
    private WebElementFacade language;

    @FindBy(xpath="//*[@id=\"content\"]/div[2]/div/ul/li[2]/a/span[2]/span[1]")
    private WebElementFacade book;

    @FindBy(xpath="//*[@id=\"download\"]/div/table/tbody/tr[2]/td[2]/a")
    private WebElementFacade bookXPath;


    public DictionaryPage() {
    }

    public void enter_keywords(String keyword) {
        searchTerms.type(keyword);
    }

    public void lookup_terms() {
        searchTerms.submit();
    }

    public void changeLanguage() {
        language.click();
    }

    public void openBook() {
        book.click();
        bookXPath.click();
    }

    public List<String> getDefinitions() {
        WebElementFacade definitionList = find(By.tagName("html"));
        return definitionList.findElements(By.tagName("span")).stream()
                .map( element -> element.getText() )
                .collect(Collectors.toList());
    }
}