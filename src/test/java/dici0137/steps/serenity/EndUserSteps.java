package dici0137.steps.serenity;

import dici0137.pages.DictionaryPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertEquals;

public class EndUserSteps {

    DictionaryPage dictionaryPage;

    @Step
    public void enters(String keyword) {
        dictionaryPage.enter_keywords(keyword);
    }

    @Step
    public void starts_search() {
        dictionaryPage.lookup_terms();
    }

    @Step
    public void should_see_definition(String definition) {
        assertEquals(true,dictionaryPage.getDefinitions().contains(definition));
    }

    @Step
    public void should_not_see_definition(String definition) {
        assertEquals(false,dictionaryPage.getDefinitions().contains(definition));
    }

    @Step
    public void is_the_home_page() {
        dictionaryPage.open();
    }

    @Step
    public void looks_for(String term) {
        enters(term);
        starts_search();
    }

    @Step
    public void changes_language() {
        dictionaryPage.changeLanguage();
    }

    @Step
    public void open_Book() {
        dictionaryPage.openBook();
    }

    public void checks_Title(String s) {
        assertEquals(true,dictionaryPage.getDriver().getTitle().equals(s));
    }
}