package dici0137.features.search;

import dici0137.steps.serenity.EndUserSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class ReadingABook {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public EndUserSteps anna;

    @Issue("#OF-1")

    @Test
    public void reading_a_book() {
        anna.is_the_home_page();
        anna.looks_for("Manual of Parliamentary Practice by Luther S. Cushing");
        anna.open_Book();
        anna.checks_Title("Manual of Parliamentary Practice, by Luther S. Cushing—A Project Gutenberg eBook");
    }

} 