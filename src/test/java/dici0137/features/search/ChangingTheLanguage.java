package dici0137.features.search;

import dici0137.steps.serenity.EndUserSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class ChangingTheLanguage {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public EndUserSteps anna;

    @Issue("#OF-1")

    @Test
    public void changing_the_language() {
        anna.is_the_home_page();
        anna.changes_language();
        anna.should_see_definition("Alguns dos nossos livros mais recentes");
    }

} 