package dici0137.features.search;

import dici0137.steps.serenity.EndUserSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
//fisierul cu datele de intrare este cel precizat pe randul urmator
@UseTestDataFrom("src\\test\\resources\\NonValid.csv")
public class NonValidChangingTheLanguage {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public EndUserSteps anna;

    //acestia sunt parametrii nostri - care se vor citi de pe fiecare linie a fisierului
    String keywords;

    @Issue("#errorLogin-1")
    @Test
    public void NonValid_changing_the_language() {
        anna.is_the_home_page();
        anna.changes_language();
        anna.should_not_see_definition(keywords);
    }

} 