package dici0137.features.search;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Pending;
import net.thucydides.core.annotations.Steps;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import dici0137.steps.serenity.EndUserSteps;

import static org.junit.Assert.assertNotEquals;

@RunWith(SerenityRunner.class)
public class SearchByKeywordStory {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public EndUserSteps anna;

    @Issue("#OF-1")
    @Test
    public void searching_by_keyword() {
        anna.is_the_home_page();
        anna.looks_for("A Short Account of the History of Mathematics");
        anna.should_see_definition("A Short Account of the History of Mathematics");
    }

} 