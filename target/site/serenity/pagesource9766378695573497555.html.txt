<html xmlns="http://www.w3.org/1999/xhtml" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:ebook="http://www.gutenberg.org/ebooks/" xmlns:marcrel="http://www.loc.gov/loc.terms/relators/" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml" lang="en_US" xml:lang="en_US" version="XHTML+RDFa 1.0"><head profile="http://a9.com/-/spec/opensearch/1.1/">
<style type="text/css">
.icon   { background: transparent url(/pics/sprite.png?1573494253) 0 0 no-repeat; }
</style>
<link rel="stylesheet" type="text/css" href="/css/pg-desktop-one.css?1573494253">
<script type="text/javascript">//<![CDATA[
var json_search     = "/ebooks/suggest/";
var mobile_url      = "http://m.gutenberg.org/ebooks/search.mobile/?query=Math";
var canonical_url   = "http://www.gutenberg.org/ebooks/search/?query=Math";
var lang            = "en_US";
var fb_lang         = "en_US"; /* FB accepts only xx_XX */
var msg_load_more   = "Load More Results…";
var page_mode       = "screen";
var dialog_title    = "";
var dialog_message  = "";
//]]></script>
<script type="text/javascript" src="/js/pg-desktop-one.js?1573494253"></script>
<link rel="shortcut icon" href="/pics/favicon">
<link rel="canonical" href="http://www.gutenberg.org/ebooks/search/?query=Math">
<link rel="search" type="application/opensearchdescription+xml" title="Search Project Gutenberg" href="http://www.gutenberg.org/catalog/osd-books.xml">
<link rel="alternate nofollow" type="application/atom+xml;profile=opds-catalog" title="OPDS feed" href="/ebooks/search.opds/?query=Math">
<link rel="apple-touch-icon" href="/pics/apple-touch-icon">
<meta name="viewport" content="width=device-width">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Language" content="en_US">
<meta name="title" content="Books: Math (sorted by popularity)">
<meta name="description" content="Project Gutenberg offers 60,689 free ebooks for Kindle, iPad, Nook, Android, and iPhone.">
<meta name="keywords" content="ebook, ebooks, free ebooks, free books, book, books, audio books">
<meta name="classification" content="public">
<meta property="og:title" content="Books: Math (sorted by popularity)">
<meta property="og:description" content="Project Gutenberg offers 60,689 free ebooks for Kindle, iPad, Nook, Android, and iPhone.">
<meta property="og:type" content="website">
<meta property="og:image" content="http://www.gutenberg.org/pics/logo-144x144.png">
<meta property="og:url" content="http://www.gutenberg.org/ebooks/search/?query=Math">
<meta property="og:site_name" content="Project Gutenberg">
<meta property="fb:app_id" content="115319388529183">
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@gutenberg_new">
<meta name="twitter:image" content="http://www.gutenberg.org/pics/logo-144x144.png">
<meta name="twitter:image:alt" content="book cover image">
<title>Books: Math (sorted by popularity) - Project Gutenberg</title>
<link rel="self" title="This Page" href="/ebooks/search/">
<link rel="next" title="Next Page" href="/ebooks/search/?start_index=26">
<meta name="totalResults" content="26">
<meta name="startIndex" content="1">
<meta name="itemsPerPage" content="25">
</head>
<body>
<div id="mw-head-dummy" class="noprint"></div>
<div id="content">
<div class="header">
<h1><span class="icon icon_"></span>Books: Math (sorted by popularity)</h1>
</div>
<div class="body">
<div>
<ul class="results">
<li class="grayed navlink">
<a class="link" href="/ebooks/authors/search/?query=Math" accesskey="0">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_author"></span>
</span>
</span>
<span class="cell content">
<span class="title">Authors</span>
<span class="subtitle">41 author names match your search.</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="grayed navlink">
<a class="link" href="/ebooks/subjects/search/?query=Math" accesskey="1">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_subject"></span>
</span>
</span>
<span class="cell content">
<span class="title">Subjects</span>
<span class="subtitle">36 subject headings match your search.</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="grayed navlink">
<a class="link" href="/ebooks/bookshelves/search/?query=Math" accesskey="2">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_bookshelf"></span>
</span>
</span>
<span class="cell content">
<span class="title">Bookshelves</span>
<span class="subtitle">One bookshelf matches your query.</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="grayed navlink">
<a class="link" href="/ebooks/search/?query=Math&amp;sort_order=title" accesskey="3">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_alpha"></span>
</span>
</span>
<span class="cell content">
<span class="title">Sort Alphabetically</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="grayed navlink">
<a class="link" href="/ebooks/search/?query=Math&amp;sort_order=release_date" accesskey="4">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_date"></span>
</span>
</span>
<span class="cell content">
<span class="title">Sort by Release Date</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="statusline">
<div class="padded">
<span>Displaying results 1–25</span>
<span class="links">
|
<a title="Go to the next page of results." accesskey="+" href="/ebooks/search/?query=Math&amp;start_index=26">Next</a>
</span>
</div>
</li>
<li class="booklink">
<a class="link" href="/ebooks/33283" accesskey="6">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_book"></span>
</span>
</span>
<span class="cell content">
<span class="title">Calculus Made Easy
</span>
<span class="subtitle">Silvanus P. Thompson</span>
<span class="extra">6996 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="booklink">
<a class="link" href="/ebooks/5740" accesskey="7">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_book"></span>
</span>
</span>
<span class="cell content">
<span class="title">Tractatus Logico-Philosophicus (German)</span>
<span class="subtitle">Ludwig Wittgenstein</span>
<span class="extra">4109 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="booklink">
<a class="link" href="/ebooks/4276" accesskey="8">
<span class="cell leftcell with-cover">
<img class="cover-thumb" src="/cache/epub/4276/pg4276.cover.small.jpg" alt="">
</span>
<span class="cell content">
<span class="title">North and South</span>
<span class="subtitle">Elizabeth Cleghorn Gaskell</span>
<span class="extra">2158 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="booklink">
<a class="link" href="/ebooks/38769" accesskey="9">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_book"></span>
</span>
</span>
<span class="cell content">
<span class="title">A Course of Pure Mathematics</span>
<span class="subtitle">G. H. Hardy</span>
<span class="extra">1477 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="booklink">
<a class="link" href="/ebooks/28233" accesskey="0">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_book"></span>
</span>
</span>
<span class="cell content">
<span class="title">Philosophiae Naturalis Principia Mathematica (Latin)</span>
<span class="subtitle">Isaac Newton</span>
<span class="extra">1423 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="booklink">
<a class="link" href="/ebooks/201" accesskey="1">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_book"></span>
</span>
</span>
<span class="cell content">
<span class="title">Flatland: A Romance of Many Dimensions (Illustrated)</span>
<span class="subtitle">Edwin Abbott Abbott</span>
<span class="extra">1381 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="booklink">
<a class="link" href="/ebooks/15114" accesskey="2">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_book"></span>
</span>
</span>
<span class="cell content">
<span class="title">An Investigation of the Laws of Thought
</span>
<span class="subtitle">George Boole</span>
<span class="extra">1075 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="booklink">
<a class="link" href="/ebooks/21076" accesskey="3">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_book"></span>
</span>
</span>
<span class="cell content">
<span class="title">The First Six Books of the Elements of Euclid</span>
<span class="subtitle">John Casey and Euclid</span>
<span class="extra">1043 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="booklink">
<a class="link" href="/ebooks/60619" accesskey="4">
<span class="cell leftcell with-cover">
<img class="cover-thumb" src="/cache/epub/60619/pg60619.cover.small.jpg" alt="">
</span>
<span class="cell content">
<span class="title">Astronomy Explained Upon Sir Isaac Newton's Principles</span>
<span class="subtitle">James Ferguson</span>
<span class="extra">898 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="booklink">
<a class="link" href="/ebooks/16713" accesskey="5">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_book"></span>
</span>
</span>
<span class="cell content">
<span class="title">Amusements in Mathematics</span>
<span class="subtitle">Henry Ernest Dudeney</span>
<span class="extra">860 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="booklink">
<a class="link" href="/ebooks/28696" accesskey="6">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_book"></span>
</span>
</span>
<span class="cell content">
<span class="title">Symbolic Logic</span>
<span class="subtitle">Lewis Carroll</span>
<span class="extra">673 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="booklink">
<a class="link" href="/ebooks/2153" accesskey="7">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_book"></span>
</span>
</span>
<span class="cell content">
<span class="title">Mary Barton</span>
<span class="subtitle">Elizabeth Cleghorn Gaskell</span>
<span class="extra">664 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="booklink">
<a class="link" href="/ebooks/36276" accesskey="8">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_book"></span>
</span>
</span>
<span class="cell content">
<span class="title">The Meaning of Relativity</span>
<span class="subtitle">Albert Einstein</span>
<span class="extra">606 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="booklink">
<a class="link" href="/ebooks/16527" accesskey="9">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_book"></span>
</span>
</span>
<span class="cell content">
<span class="title">1001 задача для умственного счета (Russian)</span>
<span class="subtitle">Sergei Aleksandrovich Rachinskii</span>
<span class="extra">595 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="booklink">
<a class="link" href="/ebooks/15238" accesskey="0">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_book"></span>
</span>
</span>
<span class="cell content">
<span class="title">Mathilda</span>
<span class="subtitle">Mary Wollstonecraft Shelley</span>
<span class="extra">562 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="booklink">
<a class="link" href="/ebooks/11483" accesskey="1">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_book"></span>
</span>
</span>
<span class="cell content">
<span class="title">The Life and Letters of Lewis Carroll (Rev. C. L. Dodgson)</span>
<span class="subtitle">Stuart Dodgson Collingwood</span>
<span class="extra">528 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="booklink">
<a class="link" href="/ebooks/394" accesskey="2">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_book"></span>
</span>
</span>
<span class="cell content">
<span class="title">Cranford</span>
<span class="subtitle">Elizabeth Cleghorn Gaskell</span>
<span class="extra">488 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="booklink">
<a class="link" href="/ebooks/12758" accesskey="3">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_book"></span>
</span>
</span>
<span class="cell content">
<span class="title">Library of the World's Best Mystery and Detective Stories</span>
<span class="extra">486 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="booklink">
<a class="link" href="/ebooks/31246" accesskey="4">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_book"></span>
</span>
</span>
<span class="cell content">
<span class="title">A Short Account of the History of Mathematics</span>
<span class="subtitle">W. W. Rouse Ball</span>
<span class="extra">480 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="booklink">
<a class="link" href="/ebooks/25447" accesskey="5">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_book"></span>
</span>
</span>
<span class="cell content">
<span class="title">Mysticism and Logic and Other Essays</span>
<span class="subtitle">Bertrand Russell</span>
<span class="extra">476 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="booklink">
<a class="link" href="/ebooks/13309" accesskey="6">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_book"></span>
</span>
</span>
<span class="cell content">
<span class="title">A First Book in Algebra</span>
<span class="subtitle">Wallace C. Boyden</span>
<span class="extra">426 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="booklink">
<a class="link" href="/ebooks/36884" accesskey="7">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_book"></span>
</span>
</span>
<span class="cell content">
<span class="title">The Mathematical Analysis of Logic</span>
<span class="subtitle">George Boole</span>
<span class="extra">417 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="booklink">
<a class="link" href="/ebooks/97" accesskey="8">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_book"></span>
</span>
</span>
<span class="cell content">
<span class="title">Flatland: A Romance of Many Dimensions</span>
<span class="subtitle">Edwin Abbott Abbott</span>
<span class="extra">397 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="booklink">
<a class="link" href="/ebooks/28513" accesskey="9">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_book"></span>
</span>
</span>
<span class="cell content">
<span class="title">The Wonders of the Invisible World
</span>
<span class="subtitle">Cotton Mather and Increase Mather</span>
<span class="extra">394 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="booklink">
<a class="link" href="/ebooks/32625" accesskey="0">
<span class="cell leftcell without-cover">
<span class="icon-wrapper">
<span class="icon icon_book"></span>
</span>
</span>
<span class="cell content">
<span class="title">A Treatise on Probability</span>
<span class="subtitle">John Maynard Keynes</span>
<span class="extra">388 downloads</span>
</span>
<span class="hstrut"></span>
</a>
</li>
<li class="statusline">
<div class="padded">
Displaying results 1–25
<span class="links">
|
<a title="Go to the next page of results." accesskey="+" href="/ebooks/search/?query=Math&amp;start_index=26">Next</a>
</span>
</div>
</li>
</ul>
</div>
</div>
<div class="footer">
</div>
</div>
<div id="fb-root"></div>
<div id="print-head" class="noscreen" style="display: none;">
<div class="center">http://www.gutenberg.org/ebooks/search/?query=Math<br><br>Project Gutenberg offers 60,689 free ebooks to download.</div>
</div>
<div id="screen-head" class="noprint">
<table>
<tbody><tr>
<td rowspan="2" id="logo">
<a href="/wiki/Main_Page" title="Go to the Main Page.">
<span class="icon icon_logo"></span>
</a>
</td>
<td id="tagline-badges" colspan="2">
<table>
<tbody><tr>
<td id="tagline">Project Gutenberg offers 60,689 free ebooks to download.</td>
<td id="paypal-badge" class="badge">
<form class="paypal-button" action="https://www.paypal.com/cgi-bin/webscr" method="post">
<div>
<input lang="en" xml:lang="en" type="hidden" name="cmd" value="_s-xclick">
<input lang="en" xml:lang="en" type="hidden" name="hosted_button_id" value="XKAL6BZL3YPSN">
<input type="image" name="submit" src="/pics/paypal/en_US.gif" title="Send us money through PayPal.">
</div>
</form>
</td>
<td id="flattr-badge" class="badge">
<a class="flattr-button" target="_blank" href="https://flattr.com/thing/509045/Project-Gutenberg" title="Send us money through Flattr.">
<span class="icon icon_flattr"></span>
</a>
</td>
</tr>
</tbody></table>
</td>
</tr>
<tr id="menubar-search">
<td id="menubar">
<a id="menubar-first" tabindex="11" accesskey="1" title="Start a new search." href="/ebooks/">Search</a>
<a tabindex="22" title="Our latest releases." href="/ebooks/search/?sort_order=release_date">Latest</a>
<a tabindex="31" title="Read the Project Gutenberg terms of use." href="/terms_of_use/">Terms of Use</a>
<a tabindex="32" href="/wiki/Gutenberg:Project_Gutenberg_Needs_Your_Donation" title="Learn why we need some money.">Donate?</a>
<a tabindex="33" accesskey="m" href="http://m.gutenberg.org/ebooks/search.mobile/?query=Math" title="Go to our mobile site.">Mobile</a>
</td>
<td id="search">
<form method="get" action="/ebooks/search/" enctype="multipart/form-data">
<table class="borderless">
<tbody><tr>
<td id="search-button-cell">
<button id="search-button" type="submit" title="Execute the search. <enter>">
<span class="icon icon_smsearch"></span>
</button>
</td>
<td id="search-input-cell">
<input id="search-input" name="query" type="text" title="Search Project Gutenberg. <s>" accesskey="s" value="Math" placeholder="Search Project Gutenberg. <s>" class="ui-autocomplete-input" autocomplete="off">
</td>
<td id="help-button-cell">
<button id="help-button" type="button" title="Open the help menu. <h>" accesskey="h">Help</button>
</td>
</tr>
</tbody></table>
</form>
</td>
</tr>
</tbody></table>
<div id="helpbox" class="hide">
<div>
<p>Enter your search terms separated by spaces,
then press &lt;Enter&gt;.
Avoid punctuation except as indicated below:</p>
<table class="helpbox">
<tbody><tr>
<th>Suffixes</th>
<th lang="en" xml:lang="en">.</th>
<td>exact match</td>
</tr>
<tr>
<th rowspan="7">Prefixes</th>
<th lang="en" xml:lang="en">a.</th>
<td>author</td>
</tr>
<tr>
<th lang="en" xml:lang="en">t.</th>
<td>title</td>
</tr>
<tr>
<th lang="en" xml:lang="en">s.</th>
<td>subject</td>
</tr>
<tr>
<th lang="en" xml:lang="en">l.</th>
<td>language</td>
</tr>
<tr>
<th lang="en" xml:lang="en">#</th>
<td>ebook no.</td>
</tr>
<tr>
<th lang="en" xml:lang="en">n.</th>
<td>ebook no.</td>
</tr>
<tr>
<th lang="en" xml:lang="en">cat.</th>
<td>category</td>
</tr>
<tr>
<th rowspan="3" style="width: 8em">
Operators
<small>Always put spaces around these.</small>
</th>
<th lang="en" xml:lang="en">|</th>
<td>or</td>
</tr>
<tr>
<th lang="en" xml:lang="en">!</th>
<td>not</td>
</tr>
<tr>
<th lang="en" xml:lang="en">( )</th>
<td>grouping</td>
</tr>
</tbody></table>
<table class="helpbox">
<tbody><tr>
<th>this query</th>
<th>finds</th>
</tr>
<tr>
<td lang="en" xml:lang="en">shakespeare hamlet</td>
<td>"Hamlet" by Shakespeare</td>
</tr>
<tr>
<td lang="en" xml:lang="en">qui.</td>
<td>"qui", not "Quixote"</td>
</tr>
<tr>
<td lang="en" xml:lang="en">love stories</td>
<td>love stories</td>
</tr>
<tr>
<td lang="en" xml:lang="en">a.shakespeare</td>
<td>by Shakespeare</td>
</tr>
<tr>
<td lang="en" xml:lang="en">s.shakespeare</td>
<td>about Shakespeare</td>
</tr>
<tr>
<td lang="en" xml:lang="en">#74</td>
<td>ebook no. 74</td>
</tr>
<tr>
<td lang="en" xml:lang="en">juvenile l.german</td>
<td>juvenile lit in German</td>
</tr>
<tr>
<td lang="en" xml:lang="en">verne ( l.fr | l.it )</td>
<td>by Verne in French or Italian</td>
</tr>
<tr>
<td lang="en" xml:lang="en">love stories ! austen</td>
<td>love stories not by Austen</td>
</tr>
<tr>
<td lang="en" xml:lang="en">jane austen cat.audio</td>
<td>audio books by Jane Austen</td>
</tr>
</tbody></table>
</div>
</div>
</div>

<ul class="ui-autocomplete ui-front ui-menu ui-widget ui-widget-content" id="ui-id-1" tabindex="0" style="display: none;"></ul><span role="status" aria-live="assertive" aria-relevant="additions" class="ui-helper-hidden-accessible"></span></body></html>